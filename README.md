# DS Sand box container


## Installation

> Clone this repo to your local machine using 

```shell
$ git clone https://gitlab.com/h3hitema/python/docker.git
```

> access the cloned repository
```shell
$ cd docker
```

> add the execution rights to the `start.sh` file

```shell
$ chmod +x start.sh
```
---

## Usage
> execute the script `start.sh`

```shell
$ ./start.sh
```

> this script contain the following code

```shell
#!/bin/bash
 
# build an image from the Dockerfile 
docker build -t monimage:v1.0 .

# stop & detete the container `test` if exists
docker stop test || true && docker rm test || true

# create the container `test` and listen to the port 8888 of the container  
docker run -p 8888:8888 -tid --name test monimage:v1.0

```

> list the running notebooks inside the container `test` to get the notebook url
```shell
docker exec -ti test jupyter notebook list
```

---

## Cheat sheet Docker

### Build
> Build an image from the Dockerfile in the current directory and tag the image


```shell
$ docker build -t <image-name:image-version> .
```

> List all images that are locally stored with the Docker Engine

```shell
$ docker image ls
```

> Delete an image from the local image store

```shell
$ docker rmi <image-name:image-version>
```

### Share

> Pull an image from a registry

```shell
$ docker pull <image-name:image-version>
```

> Retag a local image with a new image name and tag

```shell
$ docker tag <image-name:image-version> <repo>/<image-name:image-version>
```

> Push an image to a registry 

```shell
$ docker push myrepo/myimage:2.0
```

### Run


> Run a container from the `myimage` version 1.0 image, 

> name the running container

> expose port 5000 externally, mapped to port 80 inside the container. 

```shell
$ docker run --name <container-name> -p 5000:80 myimage:1.0
```

> Stop a running container through SIGTERM 

```shell
$ docker stop <container-name>
```

> Stop a running container through SIGKILL 

```shell
$ docker kill <container-name>
```

> Delete a container 

```shell
$ docker rm <container-name>
```

> List the networks

```shell
$ docker network ls
```

> List the running containers (add --a to include stopped containers) 

```shell
$ docker ps
```

> Delete all running and stopped containers

```shell
$ docker container rm -f $(docker ps -aq) 
```

> Print the last 100
lines of a container’s logs

```shell
$ docker container logs --tail 100 web
```


---

## Cheat sheet Dockerfile

### Inheritance
> Create a new image from the last ubuntu version

> must be the first non-comment instruction in the Dockerfile.

```Dockerfile
FROM ubuntu:latest
```

### Initialization

> Sets the working directory for any RUN, CMD, ENTRYPOINT, COPY, and ADD instructions that follow it.
```Dockerfile
WORKDIR </path/to/workdir>
```

> The command is run in the container shell
```Dockerfile
RUN <command>
```

> Copies new files, directories, or remote file URLs from `<src>` and adds them to the filesystem of the image at the path `<dest>`.

```Dockerfile
ADD ADD <src> <dest>
```

> Copies new files or directories from `<src>` and adds them to the filesystem of the image at the path `<dest>`..

```Dockerfile
COPY <src> [<src> ...] <dest>
```

### Variables

> The ENV instruction sets the environment variable `<key>` to the value `<value>`.
```Dockerfile
ENV <key> <value>
```

### Commands

> Informs Docker that the container listens on the specified network port(s) at runtime.

```Dockerfile
EXPOSE <port> [<port> ...]
```

> Provide defaults task for an executing container.
```Dockerfile
CMD ["<executable>","<param1>","<param2>"]
```

>Creates a mount point with the specified name and marks it as holding externally mounted volumes from native host or other containers.

```Dockerfile
VOLUME ["<path>", ...]
VOLUME <path> [<path> ...]
```

---

## Une commande qui peut vous servir  

> La commande `groupadd` permet d'ajouter un nouveau groupe au système

> La commande `gpasswd -a <username> <group-name>` permet d'ajouter l'utilisateur `<user-name>` au groupe `<group-name>`

```bash
$ sudo groupadd <group-name>
$ sudo gpasswd -a <username> <group-name>
```