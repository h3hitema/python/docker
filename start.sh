#!/bin/bash

# build an image from the Dockerfile 
docker build -t monimage:v1.0 .
# stop & detete the container `test` if exists
docker stop test || true && docker rm test || true
# create the container test and listen to the port 8888 of the container  
docker run -p 8888:8888 -tid --name test monimage:v1.0
# list running notebooks inside the container
docker exec -ti test jupyter notebook list