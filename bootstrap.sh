#!/bin/bash

#Install requirement
apt-get update
apt-get install -y vim git python3 python3-pip
apt-get clean

#remove cache files
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

#install pip dependencies
pip3 install jupyter numpy matplotlib pandas seaborn

#create requirements.txt file
pip3 freeze > requirements.txt

# git credentials
git config --global user.name "Issam Kadar"
git config --global user.email "issam@kadar.pro"
git config --global credential.helper cache

git clone https://gitlab.com/h3hitema/python/data-visualisation-statistiques-descriptives.git
git clone https://gitlab.com/h3hitema/python/objetpython.git