# Create a new image from the last ubuntu version
FROM ubuntu:latest

#set the maintainer as me
MAINTAINER issam

# Setting the working directory as /workspace
WORKDIR /workspace

# Copy the bootstrap.sh into the container at /workspace/config/
ADD bootstrap.sh ./config/bootstrap.sh

#Add execution permission to bootstrap.sh & execute the script
RUN chmod +x ./config/bootstrap.sh && ./config/bootstrap.sh

#Open the port 8888
EXPOSE 8888

#Launch jupyter server on localhost:8888
CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--allow-root"]